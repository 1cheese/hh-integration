import { AbstractRoute, route } from '../../../CorePackage/RouteModule';
import { IVacancy } from '../../../CorePackage/MainModule/interfaces/IVacancy';

type VacancyRouteParams = {
    id: IVacancy['id'];
};

@route<VacancyRouteParams>()
export class VacancyRoute extends AbstractRoute<VacancyRouteParams> {
    public static url({ id }: VacancyRouteParams = { id: ':id(\\d+)' }) {
        return `/vacancies/${id}`;
    }
}
