import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { RouteComponentProps } from 'react-router';

import css from './VacancyPage.module.css';
import { RootStoreContext } from '../../../CorePackage/MainModule/store';
import { IVacancy } from '../../../CorePackage/MainModule/interfaces/IVacancy';
import { getSalaryText } from '../../../CorePackage/MainModule/utils/getSalaryText';

type VacancyPageProps = RouteComponentProps<{ id: IVacancy['id'] }>;

export const VacancyPage = observer<VacancyPageProps>(function VacancyPage({
    match,
}) {
    const { vacancy } = React.useContext(RootStoreContext);
    const { fetchVacancy, resetVacancy, currentVacancy } = vacancy;
    const { id } = match.params;

    React.useEffect(() => {
        fetchVacancy(id);

        return () => {
            resetVacancy();
        };
    }, [fetchVacancy, resetVacancy, id]);

    if (currentVacancy === null) {
        return null;
    }

    const { name, salary, employer, area, description } = currentVacancy;

    return (
        <div className={css.component}>
            <h1 className={css.title}>{name}</h1>
            {salary && (
                <div className={css.salary}>{getSalaryText(salary)}</div>
            )}
            <div className={css.employer}>
                <div className={css.employerInfo}>
                    <div className={css.employerName}>
                        <a href={employer.url} target="_blank" rel="noreferrer">
                            {employer.name}
                        </a>
                    </div>
                    <div className={css.city}>{area.name}</div>
                </div>
                <div className={css.employerLogo}>
                    <img src={employer.logoUrls['240']} alt={employer.name} />
                </div>
            </div>
            <div
                className={css.desc}
                dangerouslySetInnerHTML={{ __html: description }}
            />
        </div>
    );
});
