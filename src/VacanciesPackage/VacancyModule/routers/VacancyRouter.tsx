import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { VacancyRoute } from '../routes/VacancyRoute';
import { VacancyPage } from '../components/VacancyPage';

type Props = RouteProps & {};

export class VacancyRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: VacancyRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={VacancyPage} />;
    }
}
