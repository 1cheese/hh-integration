import { makeAutoObservable } from 'mobx';

import { VacanciesEndpoint } from '../../../CorePackage/ApiModule/endpoints/VacanciesEndpoint';
import { IVacancy } from '../../../CorePackage/MainModule/interfaces/IVacancy';
import { RootStore } from '../../../CorePackage/MainModule/store';
import { createVacancy } from '../../../CorePackage/MainModule/utils/createVacancy';

export class VacancyStore {
    private vacancy: IVacancy | null = null;

    constructor(public root: RootStore) {
        makeAutoObservable(this);
    }

    get currentVacancy() {
        return this.vacancy;
    }

    fetchVacancy = async (vacancyId: IVacancy['id']) => {
        try {
            const vacancyResponse = await VacanciesEndpoint.getVacancyById(
                vacancyId,
            );
            this.vacancy = createVacancy(vacancyResponse);
        } catch (error) {
            console.error(error);
        }
    };

    resetVacancy = () => {
        this.vacancy = null;
    };
}
