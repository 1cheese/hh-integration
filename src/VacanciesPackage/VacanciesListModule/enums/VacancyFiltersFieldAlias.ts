export enum VacancyFiltersFieldAlias {
    AREA = 'area',
    SALARY = 'salary',
    TEXT = 'text',
}
