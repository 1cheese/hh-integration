import { AbstractRoute, route } from '../../../CorePackage/RouteModule';

@route()
export class VacanciesListRoute extends AbstractRoute {
    public static url() {
        return `/vacancies`;
    }
}
