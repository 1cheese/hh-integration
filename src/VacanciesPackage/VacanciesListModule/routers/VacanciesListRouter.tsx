import * as React from 'react';
import { Route, RouteProps } from 'react-router';

import { VacanciesListRoute } from '../routes/VacanciesListRoute';
import { VacanciesListPage } from '../components/VacanciesListPage';

type Props = RouteProps & {};

export class VacanciesListRouter extends React.Component<Props> {
    public static readonly defaultProps: Partial<Props> = {
        exact: true,
        path: VacanciesListRoute.url(),
    };

    public render() {
        return <Route {...this.props} component={VacanciesListPage} />;
    }
}
