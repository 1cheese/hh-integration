import * as React from 'react';
import { observer } from 'mobx-react-lite';

import { Paginator } from '../../../CorePackage/UIModule/Paginator';
import { RootStoreContext } from '../../../CorePackage/MainModule/store';

export const VacanciesPaginator = observer(function VacanciesPaginator() {
    const { vacancies, vacanciesFilters } = React.useContext(RootStoreContext);
    const { searchVacanciesQuery } = vacanciesFilters;
    const {
        fetchVacancies,
        numberOfPages,
        numberOfVacancies,
        currentPage,
    } = vacancies;

    const handlePageChange = async ({ selected }: { selected: number }) => {
        fetchVacancies({ ...searchVacanciesQuery, page: selected });
    };

    if (numberOfVacancies === 0) {
        return null;
    }

    return (
        <Paginator
            initialPage={currentPage}
            pageCount={numberOfPages}
            onPageChange={handlePageChange}
        />
    );
});
