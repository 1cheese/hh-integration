import css from './VacanciesListPage.module.css';
import { VacanciesFilters } from './VacanciesFilters';
import { VacanciesList } from './VacanciesList';
import { VacanciesPaginator } from './VacanciesPaginator';

export function VacanciesListPage() {
    return (
        <div className={css.component}>
            <header className={css.header}>
                <VacanciesFilters />
            </header>
            <main className={css.main}>
                <VacanciesList />
                <VacanciesPaginator />
            </main>
        </div>
    );
}
