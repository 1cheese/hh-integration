import * as React from 'react';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';

import css from './VacanciesList.module.css';
import { RootStoreContext } from '../../../CorePackage/MainModule/store';
import { VacancyRoute } from '../../VacancyModule/routes/VacancyRoute';
import { getSalaryText } from '../../../CorePackage/MainModule/utils/getSalaryText';

export const VacanciesList = observer(function VacanciesList() {
    const { vacancies } = React.useContext(RootStoreContext);
    const { vacanciesList, numberOfVacancies } = vacancies;

    if (numberOfVacancies === 0) {
        return null;
    }

    return (
        <div className={css.component}>
            {vacanciesList.map(({ id, name, area, salary, employer }) => (
                <div key={id} className={css.vacancy}>
                    <div className={css.main}>
                        <div className={css.title}>
                            <Link to={VacancyRoute.url({ id })}>{name}</Link>
                        </div>
                        <div className={css.employer}>{employer.name}</div>
                        <div className={css.city}>{area.name}</div>
                        {salary && (
                            <div className={css.salary}>
                                {getSalaryText(salary)}
                            </div>
                        )}
                    </div>
                    <div className={css.logo}>
                        <img
                            src={employer.logoUrls['90']}
                            alt={employer.name}
                        />
                    </div>
                </div>
            ))}
        </div>
    );
});
