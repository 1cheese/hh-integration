import * as React from 'react';
import { observer } from 'mobx-react-lite';

import css from './VacanciesFilters.module.css';
import { VacancyFiltersFieldAlias } from '../enums/VacancyFiltersFieldAlias';
import { RootStoreContext } from '../../../CorePackage/MainModule/store';

export const VacanciesFilters = observer(function VacanciesFilters() {
    const { areas, vacanciesFilters, vacancies } = React.useContext(
        RootStoreContext,
    );
    const { fetchAreasList, areasList } = areas;
    const {
        setFieldValueByFieldAlias,
        searchVacanciesQuery,
        getFieldValueByFieldAlias,
        isSearchButtonDisabled,
    } = vacanciesFilters;
    const { fetchVacancies } = vacancies;

    React.useEffect(() => {
        fetchAreasList();
    }, [fetchAreasList]);

    const handleFilterChange = (
        event: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>,
    ) => {
        const { value, name } = event.target;
        setFieldValueByFieldAlias(value, name as VacancyFiltersFieldAlias);
    };

    const handleFiltersSubmit = () => {
        fetchVacancies(searchVacanciesQuery);
    };

    return (
        <div className={css.component}>
            <div className={css.fieldWrapper}>
                <label htmlFor={VacancyFiltersFieldAlias.TEXT}>Вакансия</label>
                <input
                    id={VacancyFiltersFieldAlias.TEXT}
                    name={VacancyFiltersFieldAlias.TEXT}
                    placeholder={'Введите наименование вакансии'}
                    type="text"
                    value={getFieldValueByFieldAlias(
                        VacancyFiltersFieldAlias.TEXT,
                    )}
                    onChange={handleFilterChange}
                />
            </div>
            <div className={css.fieldWrapper}>
                <label htmlFor={VacancyFiltersFieldAlias.SALARY}>
                    Желаемая заработная плата, руб.
                </label>
                <input
                    id={VacancyFiltersFieldAlias.SALARY}
                    min={0}
                    name={VacancyFiltersFieldAlias.SALARY}
                    placeholder={'Введите желаемую зарплату'}
                    type="number"
                    value={getFieldValueByFieldAlias(
                        VacancyFiltersFieldAlias.SALARY,
                    )}
                    onChange={handleFilterChange}
                />
            </div>
            <div className={css.fieldWrapper}>
                <label htmlFor={VacancyFiltersFieldAlias.AREA}>
                    Регион поиска вакансии
                </label>
                <select
                    id={VacancyFiltersFieldAlias.AREA}
                    name={VacancyFiltersFieldAlias.AREA}
                    placeholder={'Введите регион поиска'}
                    value={getFieldValueByFieldAlias(
                        VacancyFiltersFieldAlias.AREA,
                    )}
                    onChange={handleFilterChange}
                >
                    <option label="" value="" />
                    {areasList.map(area => (
                        <option
                            key={area.id}
                            label={area.name}
                            value={area.id}
                        />
                    ))}
                </select>
            </div>
            <div className={css.buttonWrapper}>
                <button
                    disabled={isSearchButtonDisabled}
                    onClick={handleFiltersSubmit}
                >
                    Поиск
                </button>
            </div>
        </div>
    );
});
