import * as React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import { VacanciesFilters } from './VacanciesFilters';

Enzyme.configure({ adapter: new Adapter() });

describe('<VacanciesFilters /> component', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<VacanciesFilters />);
    });

    it('should render self and subcomponents', () => {
        expect(wrapper.length).toBe(1);

        const searchInput = wrapper.find('#text');
        expect(searchInput.length).toBe(1);
        expect(searchInput.props()).toMatchObject({
            name: 'text',
            placeholder: 'Введите наименование вакансии',
            type: 'text',
        });

        const salaryInput = wrapper.find('#salary');
        expect(salaryInput.length).toBe(1);
        expect(salaryInput.props()).toMatchObject({
            name: 'salary',
            placeholder: 'Введите желаемую зарплату',
            type: 'number',
            min: 0,
        });

        const areaInput = wrapper.find('#area');
        expect(areaInput.length).toBe(1);
        expect(areaInput.props()).toMatchObject({
            name: 'area',
            placeholder: 'Введите регион поиска',
        });
    });
});
