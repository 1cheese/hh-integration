import { makeAutoObservable } from 'mobx';

import { VacanciesEndpoint } from '../../../CorePackage/ApiModule/endpoints/VacanciesEndpoint';
import { IVacancy } from '../../../CorePackage/MainModule/interfaces/IVacancy';
import { createVacancy } from '../../../CorePackage/MainModule/utils/createVacancy';
import { ISearchVacanciesQuery } from '../../../CorePackage/MainModule/interfaces/ISearchVacanciesQuery';
import { RootStore } from '../../../CorePackage/MainModule/store';

export class VacanciesStore {
    private vacancies: IVacancy[] = [];
    private pageCount: number = 0;
    private page: number = 0;

    constructor(public root: RootStore) {
        makeAutoObservable(this);
    }

    get vacanciesList() {
        return this.vacancies;
    }

    get numberOfPages() {
        return this.pageCount;
    }

    get numberOfVacancies() {
        return this.vacanciesList.length;
    }

    get currentPage() {
        return this.page;
    }

    fetchVacancies = async (searchVacanciesQuery: ISearchVacanciesQuery) => {
        try {
            const vacanciesResponse = await VacanciesEndpoint.getVacancies(
                searchVacanciesQuery,
            );
            this.vacancies = vacanciesResponse.items.map(createVacancy);
            this.pageCount = vacanciesResponse.pages;
            this.page = vacanciesResponse.page;
        } catch (error) {
            console.error(error);
        }
    };
}
