import { makeAutoObservable } from 'mobx';

import { IArea } from '../../../CorePackage/MainModule/interfaces/IArea';
import { AreasEndpoint } from '../../../CorePackage/ApiModule/endpoints/AreasEndpoint';
import { RootStore } from '../../../CorePackage/MainModule/store';

export class AreasStore {
    private areas: IArea[] = [];

    constructor(public root: RootStore) {
        makeAutoObservable(this);
    }

    get areasList() {
        return this.areas;
    }

    fetchAreasList = async () => {
        try {
            const areasResponse = await AreasEndpoint.getRussianAreas();
            this.areas = areasResponse.areas;
        } catch (error) {
            console.error(error);
        }
    };
}
