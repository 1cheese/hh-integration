import { VacanciesFiltersStore } from './VacanciesFiltersStore';
import { RootStore } from '../../../CorePackage/MainModule/store';
import { VacancyFiltersFieldAlias } from '../enums/VacancyFiltersFieldAlias';

describe('VacanciesFiltersStore', () => {
    it('Should set filter field value', () => {
        const {
            setFieldValueByFieldAlias,
            getFieldValueByFieldAlias,
        } = new VacanciesFiltersStore(new RootStore());
        setFieldValueByFieldAlias('2', VacancyFiltersFieldAlias.AREA);
        setFieldValueByFieldAlias(100, VacancyFiltersFieldAlias.SALARY);
        setFieldValueByFieldAlias('text', VacancyFiltersFieldAlias.TEXT);

        const areaId = getFieldValueByFieldAlias(VacancyFiltersFieldAlias.AREA);
        const salary = getFieldValueByFieldAlias(
            VacancyFiltersFieldAlias.SALARY,
        );
        const text = getFieldValueByFieldAlias(VacancyFiltersFieldAlias.TEXT);

        expect(areaId).toEqual('2');
        expect(salary).toEqual(100);
        expect(text).toEqual('text');
    });
});
