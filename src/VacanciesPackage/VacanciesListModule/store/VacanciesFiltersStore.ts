import { makeAutoObservable } from 'mobx';
import { computedFn } from 'mobx-utils';

import { VacancyFiltersFieldAlias } from '../enums/VacancyFiltersFieldAlias';
import { ISearchVacanciesQuery } from '../../../CorePackage/MainModule/interfaces/ISearchVacanciesQuery';
import { RootStore } from '../../../CorePackage/MainModule/store';

export class VacanciesFiltersStore {
    private fields: Record<VacancyFiltersFieldAlias, string | number> = {
        [VacancyFiltersFieldAlias.AREA]: '',
        [VacancyFiltersFieldAlias.SALARY]: 0,
        [VacancyFiltersFieldAlias.TEXT]: '',
    };

    constructor(public root: RootStore) {
        makeAutoObservable(this);
    }

    getFieldValueByFieldAlias = computedFn(
        (fieldAlias: VacancyFiltersFieldAlias) => {
            return this.fields[fieldAlias];
        },
    );

    get searchVacanciesQuery(): ISearchVacanciesQuery {
        return {
            areaId: String(this.fields.area),
            salary: Number(this.fields.salary),
            text: String(this.fields.text),
        };
    }

    get isSearchButtonDisabled() {
        return (
            (this.fields.text as string).length === 0 ||
            (this.fields.area as string).length === 0
        );
    }

    setFieldValueByFieldAlias = (
        value: string | number,
        fieldAlias: VacancyFiltersFieldAlias,
    ) => {
        this.fields[fieldAlias] = value;
    };
}
