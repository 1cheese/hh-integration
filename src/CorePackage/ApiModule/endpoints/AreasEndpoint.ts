import { HttpMethod } from '../enums/HttpMethod';
import { MimeType } from '../enums/MimeType';
import { HttpCredentials } from '../enums/HttpCredentials';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { BASE_URL } from '../constants';
import { IArea } from '../../MainModule/interfaces/IArea';

export class AreasEndpoint {
    public static getRussianAreas(): Promise<{
        id: string;
        name: string;
        areas: IArea[];
    }> {
        return fetch(`${BASE_URL}/areas/113`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }
}
