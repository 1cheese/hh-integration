import qs from 'qs';

import { HttpMethod } from '../enums/HttpMethod';
import { MimeType } from '../enums/MimeType';
import { HttpCredentials } from '../enums/HttpCredentials';
import { ErrorHandler } from '../common/ErrorHandler';
import { ResponseHandler } from '../common/ResponseHandler';
import { BASE_URL } from '../constants';
import { IVacancyDTO } from '../../MainModule/interfaces/IVacancyDTO';
import { IPagedResponse } from '../../MainModule/interfaces/IPagedResponse';
import { ISearchVacanciesQuery } from '../../MainModule/interfaces/ISearchVacanciesQuery';
import { IVacancy } from '../../MainModule/interfaces/IVacancy';

export class VacanciesEndpoint {
    public static getVacancies(
        queryObject: ISearchVacanciesQuery,
    ): Promise<IPagedResponse<IVacancyDTO>> {
        const queryString = qs.stringify(
            {
                'text': queryObject.text,
                'area': queryObject.areaId,
                'salary':
                    queryObject.salary === 0 ? undefined : queryObject.salary,
                'page': queryObject.page,
                'search_field': 'name',
            },
            {
                encode: false,
            },
        );

        return fetch(`${BASE_URL}/vacancies?${queryString}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }

    public static getVacancyById(
        vacancyId: IVacancy['id'],
    ): Promise<IVacancyDTO> {
        return fetch(`${BASE_URL}/vacancies/${vacancyId}`, {
            method: HttpMethod.GET,
            credentials: HttpCredentials.SameOrigin,
            headers: {
                Accept: MimeType.APPLICATION_JSON,
            },
        })
            .then(ErrorHandler.commonHandler)
            .then(ResponseHandler.commonHandler)
            .catch(error => console.error(error));
    }
}
