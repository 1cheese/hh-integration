export interface IEmployer {
    logoUrls: {
        90: string;
        240: string;
        original: string;
    };
    name: string;
    url: string;
    id: string;
    trusted: boolean;
    blacklisted: boolean;
}
