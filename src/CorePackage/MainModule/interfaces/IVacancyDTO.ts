import { IAddress } from './IAddress';
import { ISalary } from './ISalary';
import { IEmployment } from './IEmployment';
import { IArea } from './IArea';
import { IEmployerDTO } from './IEmployerDTO';

export interface IVacancyDTO {
    id: string;
    description: string;
    area: IArea;
    address: IAddress;
    alternate_url: string;
    employment: IEmployment;
    salary: ISalary | null;
    archived: boolean;
    name: string;
    created_at: string;
    published_at: string;
    employer: IEmployerDTO;
}
