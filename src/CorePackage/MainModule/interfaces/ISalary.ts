export interface ISalary {
    to: number | null;
    from: number | null;
    currency: string;
    gross: boolean;
}
