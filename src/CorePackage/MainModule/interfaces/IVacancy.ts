import { IAddress } from './IAddress';
import { ISalary } from './ISalary';
import { IEmployment } from './IEmployment';
import { IEmployer } from './IEmployer';
import { IArea } from './IArea';

export interface IVacancy {
    id: string;
    description: string;
    area: IArea;
    address: IAddress;
    alternateUrl: string;
    employment: IEmployment;
    salary: ISalary | null;
    archived: boolean;
    name: string;
    createdAt: string;
    publishedAt: string;
    employer: IEmployer;
}
