export interface IPagedResponse<T> {
    items: T[];
    found: number;
    page: number;
    pages: number;
    per_page: number;
}
