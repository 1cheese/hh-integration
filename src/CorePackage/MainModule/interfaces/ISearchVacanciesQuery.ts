import { IArea } from './IArea';

export interface ISearchVacanciesQuery {
    text: string;
    areaId: IArea['id'];
    salary: number;
    page?: number;
}
