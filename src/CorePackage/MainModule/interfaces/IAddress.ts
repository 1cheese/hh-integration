export interface IAddress {
    city: string;
    street: string;
    building: string;
}
