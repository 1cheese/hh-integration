export interface IArea {
    id: string;
    name: string;
    url: string;
}
