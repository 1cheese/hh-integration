import * as React from 'react';
import { Router } from 'react-router-dom';

import { RootStore, RootStoreContext } from '../store';
import { RootRouter } from './RootRouter';
import { MainLayout } from './MainLayout';
import { browserHistory } from '../../RouteModule/utils/browserHistory';

export function App() {
    return (
        <Router history={browserHistory}>
            <RootStoreContext.Provider value={new RootStore()}>
                <MainLayout>
                    <RootRouter />
                </MainLayout>
            </RootStoreContext.Provider>
        </Router>
    );
}
