import * as React from 'react';

import css from './MainLayout.module.css';

const MainLayout: React.FC = ({ children }) => (
    <div className={css.component}>
        <div className={css.main}>{children}</div>
    </div>
);

export { MainLayout };
