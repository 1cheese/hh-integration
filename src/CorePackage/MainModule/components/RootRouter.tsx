import * as React from 'react';
import { Switch } from 'react-router-dom';

import { IndexRouter } from '../router/IndexRouter';
import { VacanciesListRouter } from '../../../VacanciesPackage/VacanciesListModule/routers/VacanciesListRouter';
import { VacancyRouter } from '../../../VacanciesPackage/VacancyModule/routers/VacancyRouter';

export function RootRouter() {
    return (
        <Switch>
            <IndexRouter />

            <VacanciesListRouter />
            <VacancyRouter />
        </Switch>
    );
}
