import { IEmployerDTO } from '../interfaces/IEmployerDTO';
import { IEmployer } from '../interfaces/IEmployer';

export function createEmployer(employerDTO: IEmployerDTO): IEmployer {
    return {
        ...employerDTO,
        logoUrls: {
            ...employerDTO['logo_urls'],
        },
    };
}
