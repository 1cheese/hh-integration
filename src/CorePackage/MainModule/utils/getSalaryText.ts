import { ISalary } from '../interfaces/ISalary';

export function getSalaryText(salary: ISalary) {
    return `${salary?.from ? 'от ' + salary.from : ''} ${
        salary?.to ? 'до ' + salary.to : ''
    } ${salary?.currency === 'RUR' ? 'руб.' : salary?.currency ?? ''}`;
}
