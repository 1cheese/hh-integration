import { IVacancyDTO } from '../interfaces/IVacancyDTO';
import { IVacancy } from '../interfaces/IVacancy';
import { createEmployer } from './createEmployer';

export function createVacancy(vacancyDTO: IVacancyDTO): IVacancy {
    return {
        ...vacancyDTO,
        alternateUrl: vacancyDTO['alternate_url'],
        createdAt: vacancyDTO['created_at'],
        publishedAt: vacancyDTO['published_at'],
        employer: createEmployer(vacancyDTO.employer),
    };
}
