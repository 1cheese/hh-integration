import * as React from 'react';

import {
    AreasStore,
    VacanciesFiltersStore,
    VacanciesStore,
} from '../../../VacanciesPackage/VacanciesListModule/store';
import { VacancyStore } from '../../../VacanciesPackage/VacancyModule/store';

export class RootStore {
    public areas: AreasStore;
    public vacanciesFilters: VacanciesFiltersStore;
    public vacancies: VacanciesStore;
    public vacancy: VacancyStore;

    constructor() {
        this.areas = new AreasStore(this);
        this.vacanciesFilters = new VacanciesFiltersStore(this);
        this.vacancies = new VacanciesStore(this);
        this.vacancy = new VacancyStore(this);
    }
}

export const RootStoreContext = React.createContext<RootStore>(new RootStore());
