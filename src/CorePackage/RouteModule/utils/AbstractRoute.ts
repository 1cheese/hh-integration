import { RouteComponentProps } from 'react-router';

import { RouteParams } from '../types/RouteParams';

export abstract class AbstractRoute<P extends RouteParams = {}> {
    public readonly props!: RouteComponentProps<P>;
}
