import { IRouteStatic } from '../interfaces';
import { RouteParams } from '../types/RouteParams';

export function route<P extends RouteParams = {}>() {
    return (constructor: IRouteStatic<P>) => {};
}
