import ReactPaginate, { ReactPaginateProps } from 'react-paginate';

import css from './Paginator.module.css';

export type PaginatorProps = {
    pageCount: number;
    initialPage: ReactPaginateProps['initialPage'];
    onPageChange: ReactPaginateProps['onPageChange'];
};

export function Paginator({
    pageCount,
    onPageChange,
    initialPage,
}: PaginatorProps) {
    return (
        <ReactPaginate
            breakClassName={css.break}
            breakLinkClassName={css.breakLine}
            containerClassName={css.container}
            pageClassName={css.page}
            pageLinkClassName={css.pageLink}
            activeClassName={css.active}
            activeLinkClassName={css.activeLink}
            previousClassName={css.previous}
            nextClassName={css.next}
            previousLinkClassName={css.previousLink}
            nextLinkClassName={css.nextLink}
            disabledClassName={css.disabled}
            previousLabel={'Назад'}
            nextLabel={'Вперед'}
            breakLabel={'...'}
            pageCount={pageCount}
            marginPagesDisplayed={1}
            pageRangeDisplayed={3}
            initialPage={initialPage}
            onPageChange={onPageChange}
        />
    );
}
